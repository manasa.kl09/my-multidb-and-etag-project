--uuid generation db script
create sequence IF NOT EXISTS ${schemaName}.global_id_sequence;

CREATE SEQUENCE IF NOT EXISTS ${schemaName}.hibernate_sequence
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

DROP FUNCTION IF EXISTS ${schemaName}.id_generator();
	
CREATE OR REPLACE FUNCTION ${schemaName}.id_generator(OUT result bigint) AS $$
DECLARE
    our_epoch bigint := 1314220021721;
    seq_id bigint;
    now_millis bigint;
    uuid varchar;
    -- the id of this DB shard, must be set for each
    -- schema shard you have - you could pass this as a parameter too
    shard_id int := 1;
BEGIN
    SELECT nextval('public.global_id_sequence') % 1024 INTO seq_id;

    SELECT FLOOR(EXTRACT(EPOCH FROM clock_timestamp()) * 1000) INTO now_millis;
    result := (now_millis - our_epoch) << 23;
    result := result | (shard_id << 10);
    result := result | (seq_id);
	uuid := substring(cast(result as VARCHAR), 1, 10);
    result := cast(uuid as bigint);
END;
$$ LANGUAGE PLPGSQL;

select ${schemaName}.id_generator();

/*
 *Create table crspng_pk_sequence
 */
CREATE TABLE IF NOT EXISTS ${schemaName}.crspng_pk_sequence
(
    entity_seq character varying(255) COLLATE pg_catalog."default" NOT NULL,
    seq_count bigint,
    CONSTRAINT crspng_pk_sequence_pkey PRIMARY KEY (entity_seq)
);

CREATE TABLE ${schemaName}.student
(
    id bigint NOT NULL,
    name character varying(255) COLLATE pg_catalog."default",
    age bigint NOT NULL,
    level bigint NOT NULL,
    etag bigint 
);

CREATE OR REPLACE FUNCTION ${schemaName}.update_student_etag() RETURNS trigger AS $update_student_etag$
DECLARE
    etag_value bigint;
BEGIN
	SELECT ${schemaName}.id_generator() INTO etag_value;
	NEW.etag:=etag_value;
	RETURN NEW;
END $update_student_etag$ LANGUAGE PLPGSQL;


DROP TRIGGER IF EXISTS student_etag_trigger on Student;
CREATE TRIGGER student_etag_trigger
BEFORE INSERT OR UPDATE ON Student
FOR EACH ROW EXECUTE PROCEDURE ${schemaName}.update_student_etag();

CREATE TABLE ${schemaName}.tree
(
    id bigint NOT NULL,
    node_name character varying(255) COLLATE pg_catalog."default",
    path character varying(255) COLLATE pg_catalog."default"
);
 









