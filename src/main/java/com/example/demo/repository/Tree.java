
package com.example.demo.repository;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TREE")
public class Tree {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", updatable = false, nullable = false)
	private Long id;

	@Column(name = "NODE_NAME")
	private String name;

	@Column(name = "PATH")
	private String path;

	/**
	 * default constructor
	 */
	public Tree() {
		// NO-arg constructor
	}

	public Tree(Tree childTree) {
		this.id = childTree.getId();
		this.name = childTree.getName();
		this.path = childTree.getPath();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	@Override
	public String toString() {
		return "Tree [id=" + id + ", name=" + name + ", path=" + path + "]";
	}

}
