package com.example.demo.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import com.example.demo.repository.Student;
import com.example.demo.repository.StudentRepository;
import com.example.demo.wrapper.StudentRequest;

@Service
@Transactional
public class StudentServiceImpl implements StudentService {

	@Autowired
	private StudentRepository studentRepository;
	
	@Autowired
	@Qualifier("mainEntityManager")
	private EntityManager mainDatabase;

	@Autowired
	@Qualifier("secondEntityManager")
	private EntityManager secondDatabase;
	
	@Override
	public List<Student> findAllStudents() {
		switchReadRepository();
		return (List<Student>) studentRepository.findAll();
	}

	@Override
	public Student findStudent(Long id) {
		switchReadRepository();
		return studentRepository.findOne(id);
	}

	@Override
	public void createStudent(StudentRequest student) {
		Student s = new Student();
		s.setAge(student.getAge());
		s.setLevel(student.getLevel());
		s.setName(student.getName());
		studentRepository.save(s);
	}

	@Override
	public void updateStudent(HttpServletRequest servletRequest, StudentRequest student, Long id) {
		String etagValue = servletRequest.getHeader("If-Match");
		if (org.springframework.util.StringUtils.isEmpty(etagValue))
			throw new HttpClientErrorException(HttpStatus.FORBIDDEN, "Forbidden");

		Student s = studentRepository.findOne(id);
		if (s == null)
			throw new HttpClientErrorException(HttpStatus.NOT_FOUND, "Resource not found");

		if (null != s && etagValue.equals(s.getEtag().toString())) {
			s.setAge(student.getAge());
			s.setLevel(student.getLevel());
			s.setName(student.getName());
			studentRepository.save(s);
		} else {
			throw new HttpClientErrorException(HttpStatus.PRECONDITION_FAILED,
					"Precondition failed, etag doesn't match the resource");
		}

	}

	@Override
	public void deleteStudent(HttpServletRequest servletRequest, Long id) {
		String etagValue = servletRequest.getHeader("If-Match");
		if (org.springframework.util.StringUtils.isEmpty(etagValue))
			throw new HttpClientErrorException(HttpStatus.FORBIDDEN, "Forbidden");

		Student s = studentRepository.findOne(id);
		if (s == null)
			throw new HttpClientErrorException(HttpStatus.NOT_FOUND, "Resource not found");

		if (null != s && etagValue.equals(s.getEtag().toString())) {
			studentRepository.delete(id);
		} else {
			throw new HttpClientErrorException(HttpStatus.PRECONDITION_FAILED,
					"Precondition failed, etag doesn't match the resource");
		}
	}

	private void switchReadRepository() {
			studentRepository = new JpaRepositoryFactory(secondDatabase).getRepository(StudentRepository.class);		
	}
}
