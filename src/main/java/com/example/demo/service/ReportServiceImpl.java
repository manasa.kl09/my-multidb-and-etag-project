package com.example.demo.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactory;
import org.springframework.stereotype.Service;

import com.example.demo.repository.Student;
import com.example.demo.repository.StudentRepository;
import com.example.demo.repository.Tree;
import com.example.demo.repository.TreeRepository;
import com.example.demo.wrapper.ReportType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;

@Service
@Transactional
public class ReportServiceImpl implements ReportService {

	@Autowired
	private StudentRepository studentRepository;

	@Autowired
	private TreeRepository treeRepository;

	@Autowired
	@Qualifier("mainEntityManager")
	private EntityManager mainDatabase;

	@Autowired
	@Qualifier("secondEntityManager")
	private EntityManager secondDatabase;

	@Autowired
	private HttpServletResponse response;

	@Override
	public void generateReport(ReportType reportType) {
		switchReadRepository();
		List<Student> studentData = (List<Student>) studentRepository.findAll();
		List<Tree> treeData = (List<Tree>) treeRepository.findAll();

		switch (reportType) {
		case CSV:
			generateCSVReport(studentData, treeData);
			break;
		case JSON:
			generateJSONReport(studentData, treeData);
		default:
			generateCSVReport(studentData, treeData);
			break;
		}
	}

	private void generateCSVReport(List<Student> studentData, List<Tree> treeData) {
		try {
			response.addHeader("Content-Type", "application/csv");
			response.addHeader("Content-Disposition", "attachment; filename=" + "report.csv");
			Writer writer = response.getWriter();
			StatefulBeanToCsv<Student> ssbc = new StatefulBeanToCsvBuilder(writer).build();
			writer.write("STUDENT DATA");
			ssbc.write(studentData);
			writer.write(System.lineSeparator());
			writer.write("TREE DATA");
			StatefulBeanToCsv<Tree> tsbc = new StatefulBeanToCsvBuilder(writer).build();
			tsbc.write(treeData);
			writer.flush();
			writer.close();
		} catch (Exception e) {
			System.out.println("Unable to generate CSV file report" + e);
		}
	}

	private void generateJSONReport(List<Student> studentData, List<Tree> treeData) {
		try {
			FileWriter file = new FileWriter("report.json");
			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
			String result = objectMapper.writeValueAsString(studentData);
			System.out.println(result);
			file.write(result);
			System.out.println("Successfully Copied JSON Object to File...");
			ResponseBuilder response = Response.ok((Object) file);
			response.header("Content-Disposition", "attachment; filename=\"employee_1415.pdf\"");
			response.build();
			file.flush();
			file.close();
		} catch (Exception e) {
			System.out.println("Unable to generate JSON file report" + e);
		} 
	}

	private void switchReadRepository() {
		studentRepository = new JpaRepositoryFactory(secondDatabase).getRepository(StudentRepository.class);
	}

}
