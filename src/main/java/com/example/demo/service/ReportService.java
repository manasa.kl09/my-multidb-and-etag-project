package com.example.demo.service;

import com.example.demo.wrapper.ReportType;

public interface ReportService {

	public void generateReport(ReportType reportType);

}
