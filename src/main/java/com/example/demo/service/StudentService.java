package com.example.demo.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.example.demo.repository.Student;
import com.example.demo.wrapper.StudentRequest;

public interface StudentService {

	public List<Student> findAllStudents();
	public Student findStudent(Long id);
	public void createStudent(StudentRequest student);
	public void updateStudent(HttpServletRequest servletRequest, StudentRequest student, Long id);
	public void deleteStudent(HttpServletRequest servletRequest, Long id);
}
