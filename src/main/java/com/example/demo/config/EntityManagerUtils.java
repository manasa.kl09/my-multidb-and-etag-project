package com.example.demo.config;

import javax.persistence.EntityManager;
import javax.ws.rs.HttpMethod;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactory;

public class EntityManagerUtils {
	@Autowired
	@Qualifier("mainEntityManager")
	private EntityManager mainDatabase;

	@Autowired
	@Qualifier("secondEntityManager")
	private EntityManager secondDatabase;

	public EntityManager getEm(String httpMethod) {
		if (httpMethod.equals(HttpMethod.POST.toString()) || httpMethod.equals(HttpMethod.PUT.toString())
				|| httpMethod.equals(HttpMethod.DELETE.toString()))
			return mainDatabase;
		if (httpMethod.equals(HttpMethod.GET.toString()))
			return secondDatabase;
		return mainDatabase;
	}

	public JpaRepositoryFactory getJpaFactory(String httpMethod) {
		return new JpaRepositoryFactory(getEm(httpMethod));
	}
}
