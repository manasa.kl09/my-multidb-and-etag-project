package com.example.demo.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.repository.Student;
import com.example.demo.service.StudentService;
import com.example.demo.wrapper.StudentRequest;

@RestController
public class StudentController {
	
	@Autowired
	private StudentService studentService;
	
	@CrossOrigin
	@GetMapping("/student")
	public @ResponseBody List<Student> getStudents() {
		return studentService.findAllStudents();
	}
	
	@CrossOrigin
	@GetMapping("/student/{id}")
	public @ResponseBody Student getStudent(@PathVariable(value = "id", required = true) Long id) {
		return studentService.findStudent(id);
	}
	
	@CrossOrigin
	@PostMapping("/student")
	@ResponseStatus(code = HttpStatus.CREATED)
	public @ResponseBody void createStudent(@RequestBody StudentRequest request) {
		studentService.createStudent(request);
	}
	
	@CrossOrigin
	@PutMapping("/student/{id}")
	public @ResponseBody void updateStudent(HttpServletRequest servletRequest, @RequestBody StudentRequest request, @PathVariable(value = "id", required = true) Long id) {
		studentService.updateStudent(servletRequest, request, id);
	}
	
	@CrossOrigin
	@DeleteMapping("/student/{id}")
	public @ResponseBody void deleteStudent(HttpServletRequest servletRequest, @PathVariable(value = "id", required = true) Long id) {
		studentService.deleteStudent(servletRequest, id);
	}
	
}
