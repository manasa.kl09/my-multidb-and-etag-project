package com.example.demo.controller;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {

	@CrossOrigin
	@GetMapping("/demo")
	public @ResponseBody String getDemo() {
		Path path = null;
		try {
			path = Paths.get(getClass().getClassLoader().getResource("demo.txt").toURI());
		} catch (URISyntaxException e1) {
			System.out.println(e1.getMessage());
		}       
		byte[] fileBytes = null;
		try {
			fileBytes = Files.readAllBytes(path);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		String data = new String(fileBytes);
		return data;
	}	
}