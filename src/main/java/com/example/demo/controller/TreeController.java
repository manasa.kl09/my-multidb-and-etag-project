package com.example.demo.controller;

import java.io.InputStream;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.yaml.snakeyaml.Yaml;

import com.example.demo.repository.Tree;
import com.example.demo.repository.TreeRepository;

@RestController
public class TreeController {
	
	@Autowired
	private TreeRepository treeRepository;
	
	private static final String ORG_ROOT_PATH = "/0/"; 
	private static final String PROD_ROOT_PATH = "/262/";
	
	/*
	 * Update the below API to accept tenant-id and node-type as ORGANIZATION/PRODUCT as request parameters.
	 * Based on the node-type set the ORG_ROOT_PATH or PROD_ROOT_PATH. Have it configured as environment variables.
	 * The yml file is located in resources, have it stored in S3-bucket and read it from S3-bucket
	 */


	@CrossOrigin
	@GetMapping("/config")
	public void getDemo() {
		Yaml yaml = new Yaml();
		InputStream inputStream = this.getClass()
		  .getClassLoader()
		  .getResourceAsStream("tree.yml");
		Map<String, Object> obj = (Map<String, Object>) yaml.load(inputStream);
		System.out.println("*****************************");
		fetchRecords(obj, 0L);	
	}	
	
	private void fetchRecords(Map<String, Object> obj, Long parentId) {
		obj.forEach((k, v) -> {
			System.out.println("Key = " + k + ", Value = " + v);
			if (v != null) {
				Tree t = new Tree();
				t.setName(k);
				t = treeRepository.save(t);
				if (parentId == 0)
					t.setPath(ORG_ROOT_PATH + t.getId());  
				else {
					Tree parent = treeRepository.findOne(parentId);
					t.setPath(parent.getPath() + "/" + t.getId());
				}
				t = treeRepository.save(t);
				System.out.println(t.toString());

				Map<String, Object> obj1 = (Map<String, Object>) v;
				fetchRecords(obj1, t.getId());
			} else {
				Tree t = new Tree();
				t.setName(k);
				t = treeRepository.save(t);
				if (parentId == 0)
					t.setPath(ORG_ROOT_PATH + t.getId());
				else {
					Tree parent = treeRepository.findOne(parentId);
					t.setPath(parent.getPath() + "/" + t.getId());
				}
				t = treeRepository.save(t);
				System.out.println(t.toString());
			}
		});
	}
}