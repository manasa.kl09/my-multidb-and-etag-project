package com.example.demo.controller;

import javax.ws.rs.QueryParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.service.ReportService;
import com.example.demo.wrapper.ReportType;

@RestController
public class ReportController {

	@Autowired
	private ReportService reportService;

	@CrossOrigin
	@GetMapping("/report")
	public @ResponseBody void generateReport(@QueryParam("reportType") ReportType reportType) {
		reportService.generateReport(reportType);
	}

}
