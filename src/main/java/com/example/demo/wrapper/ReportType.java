package com.example.demo.wrapper;

public enum ReportType {

	CSV, JSON
}
